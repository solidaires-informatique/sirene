# Sirene
Retrieves company and establishment data from the SIRENE's Insee API

## Usage

### Code

Method on Sirene's instances:
- `rawSiret(string $siret): array` to get data from the API as array
- `siret(string $siret): Etablissement` to get data from the API as `Etablissement`
- `rawSiren(string $siren): array` to get data from the API as array
- `siren(string $siren): UniteLegale` to get data from the API as `UniteLegale`

Example:
```php
$workplace = (new Sirene(apiKey: 'SUPER-SECRET-API-KEY'))->siret('13000766900018');
echo (string) $workplace->address;
```

### Get an API KEY

You'll need an account on https://portail-api.insee.fr/ (“connexion pour les externes”):

1) Create an application in https://portail-api.insee.fr/applications/creation (choose a name and fill the description field)
2) Select "Simple" in the "Sécurité" step.
3) Left the client id empty
4) Select "API Sirene" in the "Souscription" step and click on "Souscrire".
5) Select your application in https://portail-api.insee.fr/applications/mine and go to "Subscriptions"
6) Select "API Sirene", retrieve your API key.

## Issues, suggestions and contributions
Please open an issue at https://gitlab.com/solidaires-informatique/sirene/-/issues

## Dev tools
Code style verifications:
``` bash
vendor/bin/php-cs-fixer fix --dry-run -v --diff
```

Static analysis:
```bash
vendor/bin/phpstan analyse --memory-limit=250M
```

Tests:
``` bash
./vendor/bin/pest
```

To run `RealTest` that really call the Insee's API you'll need an API key from Insee :
1) Retrieve an API KEY (follow "Auth" part of README)
2) Copy `phpunit.xml.dist` to `phpunit.xml`.
3) Fill the API_KEY with your key.

## Licence
Licensed under GNU Affero General Public License v3.0 (AGPL-3.0), read more at https://choosealicense.com/licenses/agpl-3.0/
