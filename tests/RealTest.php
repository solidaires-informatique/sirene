<?php

declare(strict_types=1);

use SolidairesInformatique\Etablissement;
use SolidairesInformatique\Sirene;

const SI = [
    'siret' => '90065671100018',
    'siren' => '900656711',
    'denomination' => 'SOLIDAIRES INFORMATIQUE',
];

describe('Tests with real call to the API', function (): void {
    test("Recherche d'un établissement par son numéro Siret", function (): Etablissement {
        $key = getenv('API_KEY');

        $etablissement = (new Sirene(apiKey: $key))->siret(siret: SI['siret']);

        expect($etablissement->siret)->toBe(SI['siret'])
            ->and($etablissement->siren)->toBe(SI['siren'])
            ->and($etablissement->uniteLegale->denominationUniteLegale)->toBe(SI['denomination'])
            ->and($etablissement->etablissementSiege)->toBeTrue()
            ->and($etablissement->getHeadOfficeSiret())->toBe($etablissement->siret);

        return $etablissement;
    });

    test("Recherche d'une unité légale par son numéro Siren", function (): void {
        $key = getenv('API_KEY');

        $uniteLegale = (new Sirene(apiKey: $key))->siren(SI['siren']);
        expect($uniteLegale->denominationUniteLegale)->toBe(SI['denomination']);
    });
})->skip(fn () => empty(getenv('API_KEY')), 'Missing API KEY for tests');
