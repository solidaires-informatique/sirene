<?php

declare(strict_types=1);

use SolidairesInformatique\BadParameter;
use SolidairesInformatique\Client;
use SolidairesInformatique\ClientContract;
use SolidairesInformatique\ClientException;
use SolidairesInformatique\Etablissement;
use SolidairesInformatique\Sirene;

const API_KEY = 'SUPER-SECRET-KEY';
const SIRET = '01234567800100';
const SIREN = '012345678';
const FUTURE_DATE = '?date=2999-12-31';
const DENOMINATION = 'FLEX';
const VOIE = 'DE LA SOUS-TRAITANCE';

describe('Tests with mocked data', function (): void {
    beforeEach(function (): void {
        $this->client = Mockery::mock(new Client(API_KEY), ClientContract::class);

        $reflection = new ReflectionClass(new Sirene(API_KEY));
        $this->sirene = $reflection->newInstanceWithoutConstructor();
        $reflectionProperty = (new ReflectionObject($this->sirene))->getProperty('client');
        $reflectionProperty->setAccessible(true);
        $reflectionProperty->setValue($this->sirene, $this->client);
    });

    test("Recherche d'un établissement par son numéro Siret", function (): Etablissement {
        $this->client
            ->shouldReceive('get')
            ->with('/siret/'.SIRET.FUTURE_DATE)
            ->andReturn(json_decode(file_get_contents(__DIR__.'/data/siret.json'), true))
            ->once();

        $etablissement = $this->sirene->siret(SIRET);

        expect($etablissement->siret)->toBe(SIRET)
            ->and($etablissement->adresse->libelleVoie)->toBe(VOIE)
            ->and($etablissement->uniteLegale->denominationUniteLegale)->toBe(DENOMINATION)
            ->and($etablissement->etablissementSiege)->toBeTrue()
            ->and($etablissement->getHeadOfficeSiret())->toBe($etablissement->siret);

        return $etablissement;
    });

    test("Recherche d'une unité légale par son numéro Siren", function (): void {
        $this->client
            ->shouldReceive('get')
            ->with('/siren/'.SIREN.FUTURE_DATE)
            ->andReturn(json_decode(file_get_contents(__DIR__.'/data/siren.json'), true))
            ->once();

        $uniteLegale = $this->sirene->siren(SIREN);
        expect($uniteLegale->denominationUniteLegale)->toBe(DENOMINATION);
    });

    test('Recherche avec un SIRET trop court', function (): void {
        expect(fn () => $this->sirene->siret(''))
            ->toThrow(new BadParameter(sprintf('A SIRET should be 14 characters, %d given', 0)));
    });

    test('Recherche avec un SIREN trop court', function (): void {
        expect(fn () => $this->sirene->siren(''))
            ->toThrow(new BadParameter(sprintf('A SIREN should be 9 characters, %d given', 0)));
    });

    test("Recherche avec un code qui n'existe pas", function (): void {
        $exception = new ClientException(curl_init(), '', 404);
        $this->client
            ->shouldReceive('get')
            ->with('/siren/'.SIREN.FUTURE_DATE)
            ->andThrow($exception)
            ->once();

        expect(fn () => $this->sirene->siren(SIREN))
            ->toThrow($exception);
    });
});
