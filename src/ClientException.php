<?php

declare(strict_types=1);

namespace SolidairesInformatique;

class ClientException extends \Exception
{
    public \CurlHandle $curlHandle;

    public function __construct(\CurlHandle $curlHandle, string $message = '', int $code = 0, ?\Throwable $previous = null)
    {
        $this->curlHandle = $curlHandle;

        parent::__construct($message, $code, $previous);
    }
}
