<?php

declare(strict_types=1);

namespace SolidairesInformatique;

class BadParameter extends \Exception
{
    public function __construct(string $message = '', ?\Throwable $previous = null)
    {
        parent::__construct($message, 400, $previous);
    }
}
