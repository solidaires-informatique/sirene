<?php

declare(strict_types=1);

namespace SolidairesInformatique;

class Adresse
{
    public function __construct(
        public readonly ?string $complementAdresse,
        public readonly ?string $numeroVoie,
        public readonly ?string $indiceRepetition,
        public readonly ?string $typeVoie,
        public readonly ?string $libelleVoie,
        public readonly ?string $codePostal,
        public readonly ?string $libelleCommune,
        public readonly ?string $libelleCommuneEtranger,
        public readonly ?string $distributionSpeciale,
        public readonly ?string $codeCommune,
        public readonly ?string $codeCedex,
        public readonly ?string $libelleCedex,
        public readonly ?string $codePaysEtranger,
        public readonly ?string $libellePaysEtranger,
    ) {
    }

    /**
     * @param array<string, mixed> $data
     */
    public static function fromAPI(array $data, bool $first): self
    {
        return new self(
            $data[self::getKey('complementAdresse', $first)],
            $data[self::getKey('numeroVoie', $first)],
            $data[self::getKey('indiceRepetition', $first)],
            $data[self::getKey('typeVoie', $first)],
            $data[self::getKey('libelleVoie', $first)],
            $data[self::getKey('codePostal', $first)],
            $data[self::getKey('libelleCommune', $first)],
            $data[self::getKey('libelleCommuneEtranger', $first)],
            $data[self::getKey('distributionSpeciale', $first)],
            $data[self::getKey('codeCommune', $first)],
            $data[self::getKey('codeCedex', $first)],
            $data[self::getKey('libelleCedex', $first)],
            $data[self::getKey('codePaysEtranger', $first)],
            $data[self::getKey('libellePaysEtranger', $first)],
        );
    }

    private static function getKey(string $key, bool $first): string
    {
        return $key.($first ? '' : '2').'Etablissement';
    }

    public function __toString(): string
    {
        return "{$this->numeroVoie} {$this->typeVoie} {$this->libelleVoie}, {$this->codePostal} {$this->libelleCommune}";
    }
}
