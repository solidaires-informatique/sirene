<?php

declare(strict_types=1);

namespace SolidairesInformatique;

class AccessTokenException extends \Exception
{
}
