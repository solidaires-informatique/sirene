<?php

declare(strict_types=1);

namespace SolidairesInformatique;

interface ClientContract
{
    /**
     * @return array<string, mixed>
     *
     * @throws ClientException
     */
    public function get(string $uri): array;
}
