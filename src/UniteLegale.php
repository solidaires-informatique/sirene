<?php

declare(strict_types=1);

namespace SolidairesInformatique;

class UniteLegale
{
    public function __construct(
        public readonly ?string $etatAdministratifUniteLegale,
        public readonly bool $statutDiffusionUniteLegale,
        public readonly ?string $dateCreationUniteLegale,
        public readonly ?string $categorieJuridiqueUniteLegale,
        public readonly ?string $denominationUniteLegale,
        public readonly ?string $sigleUniteLegale,
        public readonly ?string $denominationUsuelle1UniteLegale,
        public readonly ?string $denominationUsuelle2UniteLegale,
        public readonly ?string $denominationUsuelle3UniteLegale,
        public readonly ?string $sexeUniteLegale,
        public readonly ?string $nomUniteLegale,
        public readonly ?string $nomUsageUniteLegale,
        public readonly ?string $prenom1UniteLegale,
        public readonly ?string $prenom2UniteLegale,
        public readonly ?string $prenom3UniteLegale,
        public readonly ?string $prenom4UniteLegale,
        public readonly ?string $prenomUsuelUniteLegale,
        public readonly ?string $pseudonymeUniteLegale,
        public readonly ?string $activitePrincipaleUniteLegale,
        public readonly ?string $nomenclatureActivitePrincipaleUniteLegale,
        public readonly ?string $identifiantAssociationUniteLegale,
        public readonly bool $economieSocialeSolidaireUniteLegale,
        public readonly bool $societeMissionUniteLegale,
        public readonly bool $caractereEmployeurUniteLegale,
        public readonly ?string $trancheEffectifsUniteLegale,
        public readonly ?int $anneeEffectifsUniteLegale,
        public readonly ?string $nicSiegeUniteLegale,
        public readonly ?string $dateDernierTraitementUniteLegale,
        public readonly ?string $categorieEntreprise,
        public readonly ?int $anneeCategorieEntreprise,
    ) {
    }

    /**
     * @param array<string, mixed> $data
     */
    public static function fromAPI(array $data): self
    {
        return new self(
            $data['etatAdministratifUniteLegale'],
            $data['statutDiffusionUniteLegale'] === 'O',
            $data['dateCreationUniteLegale'],
            $data['categorieJuridiqueUniteLegale'],
            $data['denominationUniteLegale'],
            $data['sigleUniteLegale'],
            $data['denominationUsuelle1UniteLegale'],
            $data['denominationUsuelle2UniteLegale'],
            $data['denominationUsuelle3UniteLegale'],
            $data['sexeUniteLegale'],
            $data['nomUniteLegale'],
            $data['nomUsageUniteLegale'],
            $data['prenom1UniteLegale'],
            $data['prenom2UniteLegale'],
            $data['prenom3UniteLegale'],
            $data['prenom4UniteLegale'],
            $data['prenomUsuelUniteLegale'],
            $data['pseudonymeUniteLegale'],
            $data['activitePrincipaleUniteLegale'],
            $data['nomenclatureActivitePrincipaleUniteLegale'],
            $data['identifiantAssociationUniteLegale'],
            $data['economieSocialeSolidaireUniteLegale'] === 'O',
            $data['societeMissionUniteLegale'] === 'O',
            $data['caractereEmployeurUniteLegale'] === 'O',
            $data['trancheEffectifsUniteLegale'],
            (int) $data['anneeEffectifsUniteLegale'],
            $data['nicSiegeUniteLegale'],
            $data['dateDernierTraitementUniteLegale'],
            $data['categorieEntreprise'],
            (int) $data['anneeCategorieEntreprise'],
        );
    }
}
