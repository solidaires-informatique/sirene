<?php

declare(strict_types=1);

namespace SolidairesInformatique;

final class Client implements ClientContract
{
    private const URL_API = 'https://api.insee.fr/api-sirene/3.11';

    private readonly \CurlHandle $handle;

    public function __construct(string $apiKey, string $accept = 'application/json')
    {
        $this->handle = curl_init();
        curl_setopt($this->handle, \CURLOPT_RETURNTRANSFER, true);
        curl_setopt($this->handle, \CURLOPT_HTTPHEADER, [
            "accept: {$accept}",
            "X-INSEE-Api-Key-Integration: {$apiKey}",
        ]);
    }

    public function get(string $uri): array
    {
        $data = $this->execute(self::URL_API.$uri);

        if (!isset($data['header']['statut'])) {
            throw new ClientException($this->handle, "Couldn't read the response status : ".json_encode($data));
        }

        if ($data['header']['statut'] !== 200) {
            throw new ClientException($this->handle, $data['header']['message'], $data['header']['statut']);
        }

        return $data;
    }

    /**
     * @param non-empty-string $url
     *
     * @return array<string, mixed>
     *
     * @throws ClientException
     */
    private function execute(string $url): array
    {
        curl_setopt($this->handle, \CURLOPT_URL, $url);

        $json = curl_exec($this->handle);
        $error = curl_errno($this->handle);
        if ($error !== 0) {
            throw new ClientException($this->handle, "cURL error {$error}");
        }

        curl_close($this->handle);

        if (!\is_string($json)) {
            throw new ClientException($this->handle, 'Unreadable response: '.$json);
        }

        $array = json_decode($json, true);

        if (!\is_array($array)) {
            throw new ClientException($this->handle, "Couldn't json_decode response: {$json}");
        }

        return $array;
    }
}
