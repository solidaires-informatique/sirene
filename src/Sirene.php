<?php

declare(strict_types=1);

namespace SolidairesInformatique;

class Sirene
{
    private const FUTURE = '2999-12-31';

    private readonly ClientContract $client;

    public function __construct(
        string $apiKey,
    ) {
        $this->client = new Client(apiKey: $apiKey);
    }

    /**
     * @return array<string, mixed>
     *
     * @throws ClientException
     */
    public function rawSiret(string $siret): array
    {
        return $this->client->get("/siret/{$siret}?date=".self::FUTURE);
    }

    /**
     * @throws ClientException
     * @throws BadParameter
     */
    public function siret(string $siret): Etablissement
    {
        if (\strlen($siret) !== 14) {
            throw new BadParameter(\sprintf('A SIRET should be 14 characters, %d given', \strlen($siret)));
        }

        return Etablissement::fromAPI($this->rawSiret($siret)['etablissement']);
    }

    /**
     * @return array<string, mixed>
     *
     * @throws ClientException
     */
    public function rawSiren(string $siret): array
    {
        return $this->client->get("/siren/{$siret}?date=".self::FUTURE);
    }

    /**
     * @throws ClientException
     * @throws BadParameter
     */
    public function siren(string $siren): UniteLegale
    {
        if (\strlen($siren) !== 9) {
            throw new BadParameter(\sprintf('A SIREN should be 9 characters, %d given', \strlen($siren)));
        }

        $data = $this->rawSiren($siren)['uniteLegale'];
        $data = array_merge($data, $data['periodesUniteLegale'][0]);

        return UniteLegale::fromAPI($data);
    }
}
