<?php

declare(strict_types=1);

namespace SolidairesInformatique;

class Etablissement
{
    public function __construct(
        public readonly string $siren,
        public readonly ?string $nic,
        public readonly string $siret,
        public readonly bool $statutDiffusion,
        public readonly ?string $dateCreation,
        public readonly ?string $trancheEffectifs,
        public readonly ?int $anneeEffectifs,
        public readonly ?string $activitePrincipaleRegistreMetiers,
        public readonly ?string $dateDernierTraitement,
        public readonly bool $etablissementSiege,
        public readonly int $nombrePeriodes,
        public readonly UniteLegale $uniteLegale,
        public readonly Adresse $adresse,
        public readonly Adresse $adresse2,
        public readonly string $activitePrincipale,
        public readonly string $nomenclatureActivitePrincipale,
        public readonly bool $caractereEmployeur,
    ) {
    }

    /**
     * @param array<string, mixed> $data
     */
    public static function fromAPI(array $data): self
    {
        return new self(
            $data['siren'],
            $data['nic'],
            $data['siret'],
            $data['statutDiffusionEtablissement'] === 'O',
            $data['dateCreationEtablissement'],
            $data['trancheEffectifsEtablissement'],
            (int) $data['anneeEffectifsEtablissement'],
            $data['activitePrincipaleRegistreMetiersEtablissement'],
            $data['dateDernierTraitementEtablissement'],
            $data['etablissementSiege'],
            $data['nombrePeriodesEtablissement'],
            UniteLegale::fromAPI($data['uniteLegale']),
            Adresse::fromAPI($data['adresseEtablissement'], true),
            Adresse::fromAPI($data['adresse2Etablissement'], false),
            $data['periodesEtablissement'][0]['activitePrincipaleEtablissement'],
            $data['periodesEtablissement'][0]['nomenclatureActivitePrincipaleEtablissement'],
            $data['periodesEtablissement'][0]['caractereEmployeurEtablissement'] === 'O',
        );
    }

    public function getHeadOfficeSiret(): string
    {
        return $this->siren.$this->uniteLegale->nicSiegeUniteLegale;
    }
}
